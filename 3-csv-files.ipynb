{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets used in the notebook.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 6-geometries\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\6-geometries\\data'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CSV files\n",
    "\n",
    "This notebook will show you the basics of reading and writing csv files. You may have used csv files in the past and thought they were Excel files because your computer is probably configured to show them with an Excel icon and open them in Excel if you double-click on one. But in reality they're plain text files that can be opened in any text editor (unlike an .xlsx file). They're commonly used to store data that'll be used by something other than Excel, because anything can read and write a csv file. Not everything can read and write an Excel file. This type of file doesn't really have anything to do with GIS, other than you can store attributes and coordinates in one if you want (which I do on a regular basis).\n",
    "\n",
    "Go ahead and use Jupyter Notebook or a text editor to open the `example.csv` file in your data folder. You'll see that it's just lines of data separated by commas (the \"c\" in csv stands for comma)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CSV module\n",
    "\n",
    "There's a simple module that comes with Python that's designed for reading and writing csv files. Unsurprisingly, it's called [`csv`](https://docs.python.org/3.6/library/csv.html). It can't open a file itself, though-- it uses the built-in Python [open()](https://docs.python.org/3.6/library/functions.html#open) function.\n",
    "\n",
    "Because this has nothing to do with ArcGIS, the `arcpy.env.workspace` has no effect so you can't use it to tell Python where to look for files. You need to either use full paths to files or set the **Python** working directory using `os.chdir(folder)`. Once you've done that, built-in Python functions will look in that folder for files.\n",
    "\n",
    "Let's import the modules and set the Python working directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the csv and os modules.\n",
    "import csv\n",
    "import os\n",
    "import classtools\n",
    "\n",
    "# Set the Python working directory to your data folder.\n",
    "os.chdir(data_folder)\n",
    "\n",
    "# Tell ArcGIS to look in your assignment folder for modules so that\n",
    "# it can find classtools. Jupyter will already look there and \n",
    "# doesn't actually need this.\n",
    "import sys\n",
    "sys.path.append(os.path.dirname(data_folder))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now open the file using the Python `Open()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the file. I like to call it fp (short for file pointer).\n",
    "# The 'r' stands for read. This file is opened for reading\n",
    "# but not writing.\n",
    "fp = open(file='example.csv', mode='r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading data\n",
    "\n",
    "Once you have a file open, then you can use it to create either a csv [reader](https://docs.python.org/3.6/library/csv.html#csv.reader) or [writer](https://docs.python.org/3.6/library/csv.html#csv.writer). This example creates a reader."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a csv reader from the file.\n",
    "reader = csv.reader(fp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have the reader, you can iterate over the rows in the file. As you loop over them, **each row is returned as a list of strings** (the reader makes no attempt to figure out data types, so everything stays as string). This loops over the rows in our reader and prints each one out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for row in reader:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you've iterated over a file once, you can't do it again without setting it back to the beginning. For example, let's try printing the rows again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for row in reader:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nothing happened, because the reader is looking at the end of the file. Let's use `seek` to set it back to the beginning and try again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the file back to the beginning. This is only\n",
    "# necessary if you've already read past the beginning,\n",
    "# not if you've just opened the file.\n",
    "fp.seek(0)\n",
    "\n",
    "for row in reader:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Header rows and converting data types\n",
    "\n",
    "Sometimes you'll want to skip the first row because it's a header line and not real data. You can do that with `next()`, which gets the next item in an iterator. You also might need to convert the data to numbers since everything comes in as a string when you use the csv module. \n",
    "\n",
    "This next example skips the header row and uses a list comprehension to convert the data to numbers. Note that it only works if all of the data are numeric! (And it would crash if you didn't skip the header row, because those can't be converted to numbers.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the file back to the beginning because\n",
    "# we've been messing with it.\n",
    "fp.seek(0)\n",
    "\n",
    "# Get the next row in the file (in this case it's the first\n",
    "# row, since we just set the file back to the beginning).\n",
    "headers = next(reader)\n",
    "print('Headers:', headers)\n",
    "\n",
    "print('Data:')\n",
    "for row in reader:\n",
    "    # Convert the list of strings (row) to a list of floats.\n",
    "    floats = [float(x) for x in row]\n",
    "    print(floats)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Closing files\n",
    "\n",
    "Closing files is especially important when writing data to the files, because it makes sure that the data gets written to disk. But it's still a good idea to close files that are open for reading. Let's close the one you've been working with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fp.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Automatically closing files\n",
    "\n",
    "Python will automatically close files for you if you use a `with` statement when opening them. The syntax looks like this:\n",
    "\n",
    "```python\n",
    "with open(...) as variable_name:\n",
    "    # Any code that uses the file needs to be indented under here.\n",
    "# Once code isn't indented anymore, the file is closed and no longer available.\n",
    "```\n",
    "\n",
    "All of the code that uses the file must be in a single cell if you use this syntax, however, because you can carry an indentation across code cells.\n",
    "\n",
    "This is how you could use this syntax to print all of the rows in the example.csv file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the file for reading and call it fp.\n",
    "with open(file='example.csv', mode='r') as fp:\n",
    "    \n",
    "    # Create a csv reader.\n",
    "    reader = csv.reader(fp)\n",
    "    \n",
    "    # Loop through all of the rows and print them out.\n",
    "    for row in reader:\n",
    "        print(row)\n",
    "        \n",
    "# No indentation means the the file is closed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the data\n",
    "\n",
    "Here's a complete example that creates a multipoint from the coordinates in the file you've been playing with. The comments explain what's going on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import arcpy because we haven't yet.\n",
    "import arcpy\n",
    "\n",
    "# Create an array to hold the coordinates.\n",
    "points = arcpy.Array()\n",
    "\n",
    "# Open the file for reading.\n",
    "with open(file='example.csv', mode='r') as fp:\n",
    "    \n",
    "    # Create a csv reader.\n",
    "    reader = csv.reader(fp)\n",
    "\n",
    "    # Skip the header row.\n",
    "    next(reader)\n",
    "\n",
    "    # Loop through the remaining rows.\n",
    "    for row in reader:\n",
    "\n",
    "        # Get the x and y coordinates out of the list using\n",
    "        # their indexes. Arcpy is smart enough to convert them\n",
    "        # to numbers for us when we create the point.\n",
    "        x = row[2]\n",
    "        y = row[3]\n",
    "\n",
    "        # Add a point to the array.\n",
    "        points.add(arcpy.Point(x, y))\n",
    "\n",
    "# Now that we've looped over all of the rows in the csv file,\n",
    "# so all of the points are in the array, create a multipoint.\n",
    "# (The file is now closed.)\n",
    "multipoint = arcpy.Multipoint(points)\n",
    "\n",
    "# Show the multipoint.\n",
    "multipoint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DictReaders\n",
    "\n",
    "There's another type of csv reader called a [DictReader](https://docs.python.org/3.6/library/csv.html#csv.DictReader) (for dictionary reader). Instead of returning lists of data, it returns python *dictionaries*. Dictionaries allow you to access data by keyword. Let's look at an example to illustrate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the file for reading and create a csv dictreader.\n",
    "fp = open(file='example.csv', mode='r')\n",
    "reader = csv.DictReader(fp)\n",
    "\n",
    "# Get the first row and print it out.\n",
    "row = next(reader)\n",
    "row"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that this time when you printed the first row, it wasn't the column headers. In fact, there's no reason to skip the first row anymore because the DictReader automatically uses it to name the data values in the other rows. You can access the values in this `row` variable by name instead of by position in the list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(row['x'])\n",
    "print(row['y'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's close this file now that you've seen what a DictReader does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fp.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now change the point example to use a DictReader:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an array to hold the coordinates.\n",
    "points = arcpy.Array()\n",
    "\n",
    "# Open the file for reading.\n",
    "with open(file='example.csv', mode='r') as fp:\n",
    "\n",
    "    # Create a csv dictreader.\n",
    "    reader = csv.DictReader(fp)\n",
    "\n",
    "    # Loop through the rows.\n",
    "    for row in reader:\n",
    "\n",
    "        # Get the x and y coordinates out of the dictionary using\n",
    "        # their names. Arcpy is smart enough to convert them\n",
    "        # to numbers for us when we create the point.\n",
    "        x = row['x']\n",
    "        y = row['y']\n",
    "\n",
    "        # Add a point to the array.\n",
    "        points.add(arcpy.Point(x, y))\n",
    "\n",
    "# Now that we've looped over all of the rows in the csv file,\n",
    "# so all of the points are in the array, create a multipoint.\n",
    "# (The file is now closed.)\n",
    "multipoint = arcpy.Multipoint(points)\n",
    "\n",
    "# Show the multipoint.\n",
    "multipoint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing data\n",
    "\n",
    "If you want to write instead of read csv data, you need to open the file for writing. Remember that you used `'r'` earlier, to open the file for reading? Now you can use either `'w'` to create a new file (**be careful, because this will overwrite the file if already exists, and it won't warn you**) or `'a'` to append to an existing file. With Python 3, you need to add `newline=''` unless you want the file to be double-spaced. \n",
    "\n",
    "Let's create a new file for writing: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fp = open(file='example2.csv', mode='w', newline='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use the `fp` variable to create a csv [writer](https://docs.python.org/3.6/library/csv.html#csv.writer):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "writer = csv.writer(fp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And putting data into the file is as easy as passing a **list** of values to the writer's `writerow()` function. Let's pass some column headers for the first line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "headers = ['id', 'name']\n",
    "writer.writerow(headers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, you could've shrunk that code a bit and done it like this:\n",
    "\n",
    "```python\n",
    "writer.writerow(['id', 'name'])\n",
    "```\n",
    "\n",
    "Now let's add a couple of rows of data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "writer.writerow([1, 'Doug'])\n",
    "writer.writerow([2, 'Debbie'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(The numbers it's printing out are a count of the number or characters written to file in the last call to `writerow()`. It looks like it's counting wrong, but line endings count for two characters on Windows.)\n",
    "\n",
    "There's a good chance that data hasn't actually been written to disk yet and is still in memory. The data will be forced out to disk if you close the file, though."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fp.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Go ahead and open example2.csv in Jupyter Notebook in order to see what it looks like."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 1\n",
    "\n",
    "Use a **csv reader** to print out all of the rows in the example2.csv file you just created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your output should've looked like this:\n",
    "\n",
    "```\n",
    "['id', 'name']\n",
    "['1', 'Doug']\n",
    "['2', 'Debbie']\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's how you could use the `with` syntax and append to the file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the file for APPENDING and call it fp.\n",
    "with open(file='example2.csv', mode='a', newline='') as fp:\n",
    "    \n",
    "    # Create a csv writer.\n",
    "    writer = csv.writer(fp)\n",
    "    \n",
    "    # Write some data.\n",
    "    writer.writerow([3, 'Sara'])\n",
    "    writer.writerow([4, 'Ben'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time there was no need to use `fp.close()` because the file was automatically closed when Python got to the end of the indented code. Let's take a look at the contents of the file now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the file for reading.\n",
    "with open(file='example2.csv', mode='r') as fp:\n",
    "    reader = csv.reader(fp)\n",
    "    for row in reader:\n",
    "        print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pandas\n",
    "\n",
    "Another easy way to work with csv files is to use the [pandas](https://pandas.pydata.org/) module. Unlike the csv module, this one doesn't come with Python. It does, however, come with ArcGIS Pro, so you have it already.\n",
    "\n",
    "You can use pandas to read a csv file into a dataframe, which is structure for holding data. Let's do it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# It's kind of convention to rename pandas to pd when importing it.\n",
    "import pandas as pd\n",
    "\n",
    "# Read the contents of example.csv into a variable called df (short for dataframe).\n",
    "df = pd.read_csv('example.csv')\n",
    "\n",
    "# Look at the result.\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that Pandas automatically converted the values to numbers for you.\n",
    "\n",
    "You can do a lot of powerful data manipulations with Pandas dataframes, and I use them almost every day. Just to be clear, though, basic dataframes aren't used for spatial data (although you can use other modules such as [arcgis](https://developers.arcgis.com/python/) and [geopandas](http://geopandas.org/) to add spatial abilities to them).\n",
    "\n",
    "You have to use different syntax if you want to loop through the rows in a dataframe. There are multiple ways to do it, and I'll show you the `itertuples` method. Each `row` variable inside the loop is now a *named tuple*, and you can get the values out of it by using their names. First let's just print each one out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for row in df.itertuples():\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's print out the x and y values for each row, instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for row in df.itertuples():\n",
    "    print(row.x, row.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So let's try the multipoint example with a pandas dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an array to hold the coordinates.\n",
    "points = arcpy.Array()\n",
    "\n",
    "# Read the csv file into a pandas dataframe.\n",
    "df = pd.read_csv('example.csv')\n",
    "\n",
    "# Loop through the rows.\n",
    "for row in df.itertuples():\n",
    "    \n",
    "    # Add a point to the array.\n",
    "    points.add(arcpy.Point(row.x, row.y))\n",
    "    \n",
    "# Now that we've looped over all of the rows in the csv file,\n",
    "# so all of the points are in the array, create a multipoint.\n",
    "multipoint = arcpy.Multipoint(points)\n",
    "\n",
    "# Show the multipoint.\n",
    "multipoint"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

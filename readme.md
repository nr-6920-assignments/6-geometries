# Geometries

This week you'll learn how to construct and use your own geometry objects.

See the [homework description](markdown/homework.md).

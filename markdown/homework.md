# Geometries homework

*Due Monday, March 1, at midnight*

## Notebook comprehension questions

*Worth 16 points*

There is no ArcGIS project this time because the notebooks don't use any data that would draw on a map in ArcGIS.

Work through the notebooks in this order:

- 1-creating-geometries.ipynb (8 points)
- 2-using-geometries.ipynb (6 points)
- 3-csv-files.ipynb (2 points)

## Script problems

*Worth 10 points each*

If you want full credit:

1. If you need to use the value of one of the variables that I tell you to add to the first cell, use the variable! Your notebook should not have those values anywhere except in the first cell.
2. Include explanations of what your code does.
3. Include all plots or anything else that I ask for.

The first part of [problem 1](problem1.md) is a variation on the example code in one of the notebooks, and then you build on that and add a bit of conditional logic.

The first part of [problem 2](problem2.md) is a variation on the first part of problem 1, and then you build on that and add a bit of conditional logic (again).

[Problem 3](problem3.md) is pretty different, but I've given you an outline of what needs to happen.
 


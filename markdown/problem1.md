# Problem 1

## To get full credit:

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.

## Directions

Create a notebook called `problem1`. Set up four variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
filename = r'D:\classes\NR6920\Assignments\6-geometries\data\sites.csv'
input_srs = 'WGS 1984 UTM Zone 12N'
output_srs = 'WGS 1984'
cover_type = 'trees'
```

Nothing else should be in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

This one is a little different than what you've done in the past. I want your notebook to have several sections, and they'll all build on a previous section. **Use a markdown cell to add a header to each section so that I can tell what's going on.** To do that, create a new cell and make sure it's the active one, and then change the pulldown menu at the top of the page so it says "Markdown" instead of "Code". Then inside the cell, add a heading like this:

```
# Part 1

Put any other text you want here.
```

Run the cell and you'll see that whatever is after the "#" turns into a header.

## Data

There's a file called `sites.csv` in your `6-geometries\data` folder. The first few lines of the file look like this:

```
id,cover,count,x,y
1,shrubs,3,455552.41836086405,4641822.053684879
2,trees,4,449959.8408513342,4633802.508576875
3,rocks,2,441201.6534307497,4619029.662325286
```

## Part 1

1. Have your notebook open the csv file specified by `filename` and create a **single multipoint geometry** from the coordinates in the file. Assign the spatial reference that's in the `input_srs` variable. The only difference between this and the example in the csv-files notebook is that you're using a variable for the filename and you need to assign a spatial reference when creating the geometry. That's only two lines of code that you need to change from the example!

2. Use the following code to plot your multipoint geometry, except change `geom1` to whatever you called your geometry.

```python
m1 = classtools.make_map(place=[41.75, -111.25], geom=geom1, zoom=9)
m1
```

3. Use `m1.take_screenshot()` to take a screenshot of your map for my benefit.

4. Now add a cell with this code, except change `geom1` to whatever you called your multipoint geometry.

```python
print('SRS:', geom1.spatialReference.name)
print('Points:', geom1.pointCount)
print('First point:', geom1.getPart(0))
```

If the output from that doesn't look like this, then you've done something wrong:

```
SRS: WGS_1984_UTM_Zone_12N
Points: 42
First point: 455552.418360864 4641822.05368488 NaN NaN
```

## Part 2

1. Reproject your multipoint from Part 1 to the SRS stored in the `output_srs` variable. This is one line of code. Store this in a different variable-- don't overwrite your existing geometry variable. For example, if your geometry from Part 1 was in a variable called `geom1`, don't also store your new projected geometry in a variable called `geom1`.

2. Now add a cell with this code, except change `geom2` to whatever you called your new reprojected multipoint geometry from Part 2.

```python
print('SRS:', geom2.spatialReference.name)
print('Points:', geom2.pointCount)
print('First point:', geom2.getPart(0))
```

Expected output:

```
SRS: GCS_WGS_1984
Points: 42
First point: -111.536080100302 41.9271075694626 NaN NaN
```

## Part 3

1. Copy your code from Part 1 and modify it so that it only uses the points that have the cover type specified with the `cover_type` variable. So if `cover_type = 'trees'`, then your multipoint geometry will only include the rows from the file where the `cover` column is "trees". After you've created your new multigeometry, reproject it to the spatial reference in `output_srs`. Again, do not overwrite your geometry variables from Parts 1 or 2. (See the *Hints* section at the end of these instructions for some help.)
   1. Students tend to make this a lot harder than it is and don't take my advice to just copy and modify their code from part 1. If you take that and then add your one line of code from part 2, you have code that creates a multipoint using all of the points and then reprojects it.
   2. Now to make it only include the points with the right cover type, you only need to add ONE line of code (two if you want to store the point's cover type in a variable). You will have to indent one or more other lines of code, but not change them otherwise, except for changing the name of your geometry.
   3. See the *Questions to ask yourself* section at the end of these instructions for some help thinking through it.

2. Use the following code to plot your geometries, except change `geom1` to whatever you called your first geometry from Part 1, and `geom3` to whatever you called your new, subsetted multipoint geometry from Part 3. This will plot your first geometry in light gray, and your new geometry that only includes trees in red.

```python
m2 = classtools.make_map(place=[41.75, -111.25], geom=geom1, zoom=9, color=[0, 0, 0, 50])
classtools.add_geom(m2, geom3)
m2
```

3. Use `m2.take_screenshot()` to take a screenshot of your map for my benefit.

4. Now add a cell with this code, except change `geom3` to whatever you called your new projected multipoint geometry from Part 3.

```python
print('SRS:', geom3.spatialReference.name)
print('Points:', geom3.pointCount)
print('First point:', geom3.getPart(0))
```


Expected output:

```
SRS: GCS_WGS_1984
Points: 11
First point: -111.602848980692 41.8545461086493 NaN NaN
```

### Questions to ask yourself:
 
1. How do you get the cover type for each row? Could it be similar to how you get the x and y coordinates from the row?
2. Once you have the cover type, how do you figure out if it's the same as `cover_type`?
3. What should you do if the row has the correct cover type?
4. Do you need to do anything if the row does not have the correct cover type?

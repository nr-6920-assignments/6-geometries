# Problem 3

## To get full credit:

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.

## Directions

Create a notebook called `problem3`. Set up four variables in the very first code cell, with these names and values (except change the path to the 6-geometries folder on **your** computer: 

```python
folder = r'D:\classes\NR6920\Assignments\6-geometries' # NOT the data folder!
input_filename = r'data\elwha_utm.shp'
output_filename = 'transect.csv'
distance = 1000
```

Nothing else should be in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

*Notice that the `folder` variable doesn't point to your data folder-- that's so that the csv file gets created in your top-level folder instead of in the data folder. I have your git repos set up to ignore new files in your data data folder when you turn things in, and I want you to turn this csv file in. Because you'll use the `folder` variable when setting your workspace, you need to add the data folder to the `input_filename` path so arcpy knows to look in that subfolder of the workspace.*

This problem's a little different. You're going to create a csv file (`output_filename`) that contains coordinates at a set interval along a line (the Elwha River in Washington State, because it's one of my favorite places in the world, although I never make it back to visit anymore). So for example, if `distance` is 1000 and the units are meters (we'll just assume all of the units are the same), then you'll get the coordinates of the points every 1000 meters on the line contained in `input_filename`, and save them to a csv file. See the [problem3-animation.gif](../problem3-animation.gif) animated gif for an illustration of what I mean.

- Don't forget to add a header row to your csv (csv notebook, writing data section).
- Name your columns id, x, and y. This is so the checks will work correctly. 
- Add an ID for each point, in order **starting with 1**.
- Include the x and y coordinates for each point (creating geometries notebook, right before problem 1).
- You'll want to keep track of your current distance along the line and id numbers. Use a `while` loop to keep creating points until you've traversed the entire length of the line.

If you're using ArcGIS instead of Jupyter, you can make sure the csv file gets created in the correct location by adding this code at the same time you import arcpy and set your workspace (it sets the current Python directory, which in Jupyter would be the same folder as the notebook, but ArcGIS might mess with that):

```python
import os
os.chdir(folder)
```

1. You don't know how to get a line out of a shapefile yet (next week!). So use this code:

```python
line = next(arcpy.da.SearchCursor(input_filename, 'shape@'))[0]
```

2. Plot the line with this code before creating the transect points.

```python
m1 = classtools.make_map(place=[47.9, -123.43], geom=line, zoom=9)
m1
```

3. Then use `m1.take_screenshot()` so that I have a pretty map to look at.

4. See the flowchart shown in [problem3.pdf](../problem3.pdf). Here's the same thing in outline form (pseudocode):
   1. Create a csv writer.
   2. Create a variable to hold the point id and initialize it with 1.
   3. Create a variable to hold the current distance along the line and initialize it with 0.
   4. While the current distance along line < length of line:
      1. Get the point at the current distance along the line (see the [`positionAlongLine` method on geometries](https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/geometry.htm#:~:text=type.-,positionAlongLine%20(value%2C%20%7Buse_percentage%7D),-Returns)). This method lives on a line geometry, and the `value` parameter is a distance. The method returns a point geometry.
      2. Get the coordinates of the point from the last step.
      3. Write the id and coordinates to the csv file.
      4. Increment the id variable. What should you add to its value in order to get the next id to use?
      5. Increment the current distance variable. What should you add to its value to get the next distance for your next transect point?

5. Use this code to count the number of rows in your new csv and look at the first five of them.

```python
df = classtools.table2pd(output_filename)
print('Rows:', len(df))
df.head()
```

Expected output:

```
Rows: 72
```

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>x</th>
      <th>y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>456304.580187</td>
      <td>5.291247e+06</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>456858.779208</td>
      <td>5.290438e+06</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>457366.207890</td>
      <td>5.289606e+06</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>457812.236866</td>
      <td>5.288729e+06</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>458595.691761</td>
      <td>5.288289e+06</td>
    </tr>
  </tbody>
</table>

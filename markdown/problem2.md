# Problem 2

## To get full credit:

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.

## Directions

Divide this notebook up into sections just like you did with problem 1.

Create a notebook called `problem2`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
filename = r'D:\classes\NR6920\Assignments\6-geometries\data\track.csv'
srs = 'WGS 1984 UTM Zone 12N'
month = 4
```

Nothing else should be in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

## Data

There's a file called `track.csv` in your `6-geometries\data` folder. It contains GPS location data (for a deer, if I remember correctly). The first few lines of the file look like this:

```
id,date,x,y
302,20110402,473226.1307710044,4429257.122467113
303,20110402,472867.9297945726,4428476.929777273
304,20110402,472872.6206456687,4428487.89016115
```

## Part 1

1. Have your notebook open the csv file specified with `filename` and create a line object from the coordinates in the file. Assign the spatial reference specified with the `srs` variable. You can pretty much take your code from the first part of problem 1 and tweak it. (Again, make your life easier and don't ignore my advice.)
   1. In problem 1 you made a multipoint. What type of geometry are you making for this problem? 
   2. Look at the creating-geometries notebook and compare the methods for making these two types of geometries. If you start with your problem 1 code, what do you need to change in order to make the geometry type for this problem?

2. Use the following code to plot your line geometry, except change `geom1` to whatever you called your geometry.

```python
m1 = classtools.make_map(place=[40.01, -111.31], geom=geom1, zoom=14)
m1
```

3. Use `m1.take_screenshot()` for my benefit. Make sure you I can see some of the labels on the map so that I know your line is in the right place.

4. Now add a cell with this code, except change `geom1` to whatever you called your line geometry.

```python
print('SRS:', geom1.spatialReference.name)
print('Parts:', geom1.partCount)
print('Points:', geom1.pointCount)
print('Length:', geom1.length)
```

Expected output:

```
SRS: WGS_1984_UTM_Zone_12N
Parts: 1
Points: 147
Length: 69875.80315267277
```

## Part 2

1. Copy your code from part 1 and modify it so that it only uses the points from the month specified with the `month` variable. The dates in the file look like 'YYYYMMDD', where YYYY is the year, MM is the month, and DD is the day (e.g. '20110402'). If you put one of these date strings into a variable called `mydate`, you can find the month by pulling out two characters starting at index 4, like `mydate[4:6]`. This will be a string, so convert it to an integer and then you can compare it to `month` to see if it should be added to the line. 
   1. Students tend to make this one infinitely harder than it is and don't take my advice to just copy and modify their code from part 1 or to answer the questions at the end of the problem description. Answering them will walk you through what you need to do.
   2. It's possible to do this problem by adding ONE line of code to your problem 1 answer (and indenting others), but you'll probably want to use some intermediate variables, in which case you'll add more than one line. But if you find yourself wanting to do more than create a couple new variables and add one `if` statement, you're making it WAY too hard.

2. Use the following code to plot your geometries, except replace `geom1` with whatever you called your geometry from Part 1, and `geom2` with whatever you called your geometry from Part 2. This will draw your original line in gray, and your new, subsetted one, on top in red.

```python
m2 = classtools.make_map(place=[40.01, -111.31], geom=geom1, zoom=14, color=[0, 0, 0, 50])
classtools.add_geom(m2, geom2)
m2
```

3. Use m2.take_screenshot() for my benefit.

4. Now add a cell with this code, except change `geom2` to whatever you called your line geometry from Part 2.

```python
print('SRS:', geom2.spatialReference.name)
print('Parts:', geom2.partCount)
print('Points:', geom2.pointCount)
print('Length:', geom2.length)
```

Expected output:

```
SRS: WGS_1984_UTM_Zone_12N
Parts: 1
Points: 78
Length: 36220.21314001991
```

### Part 2 questions to ask yourself

Answer these in order and they'll walk you through making the needed changes for part 2.

1. How do you get date value for each row? 
2. Once you have the date string from the row, how do the instructions tell you to get the month string?
3. Once you have the month string, how do you convert it to a number so you can compare it to the `month` variable? Hint: See [here](https://docs.python.org/3.7/library/functions.html#int), and there are examples in the strings and numbers notebooks from the second week.
4. What should you do if the row has the correct month?
5. Do you need to do anything if the row does not have the correct month? 

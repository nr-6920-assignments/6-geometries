import os
from tests.testtools import BaseTester, Runner

__unittest = True

def test_results(problem):
    if problem == 1:
        Runner.print('This problem has no output to test')
    elif problem == 2:
        Runner.print('This problem has no output to test')
    elif problem == 3:
        Runner.test_output(3, Problem3R)

def test_notebook(problem):
    if problem == 1:
        if Runner.run_problem(1, [Problem1A, Problem1B]):
            Runner.print(
                'Check tests/problem1b.ipynb to make sure that part 3 output looks like this:\n'
                + 'SRS: GCS_WGS_1984\n'
                + 'Points: 6\n'
                + 'First point: -123.536080100302 41.9271075694626 NaN NaN\n')
    elif problem == 2:
        if Runner.run_problem(2, [Problem2A, Problem2B]):
            Runner.print(
                'Check tests/problem2b.ipynb to make sure that part 2 output looks like this:\n'
                + 'SRS: WGS_1984_UTM_Zone_10N\n'
                + 'Parts: 1\n'
                + 'Points: 69\n'
                + 'Length: 32688.479510991376 a length of 32688.479510991376\n')
    elif problem == 3:
        Runner.run_problem(3, [Problem3A, Problem3B])


class Problem1(BaseTester):
    problem = 1
    args = {}
    filename = 'sites_test.csv'
    output_srs = 'WGS 1984'
    input_srs = ''
    cover_type = ''

    def setUp(self):
        super().setUp()
        self.args = dict(
            filename=os.path.join(self.data_folder, self.filename),
            input_srs=self.input_srs,
            output_srs=self.output_srs,
            cover_type=self.cover_type,
        )


class Problem1A(Problem1):
    suffix = 'a'
    input_srs = 'WGS 1984 UTM Zone 12N'
    cover_type = 'trees'


class Problem1B(Problem1):
    suffix = 'b'
    input_srs = 'WGS 1984 UTM Zone 10N'
    cover_type = 'shrubs'


class Problem2(BaseTester):
    problem = 2
    args = {}
    filename = 'track_test.csv'
    srs = ''
    month = 0

    def setUp(self):
        super().setUp()
        self.args = dict(
            filename=os.path.join(self.data_folder, self.filename),
            srs=self.srs,
            month=self.month,
        )


class Problem2A(Problem2):
    suffix = 'a'
    srs = 'WGS 1984 UTM Zone 12N'
    month = 4


class Problem2B(Problem2):
    suffix = 'b'
    srs = 'WGS 1984 UTM Zone 10N'
    month = 5


class Problem3(BaseTester):
    problem = 3
    args = {}
    input_filename = 'data/elwha_test.shp'
    output_filename = 'transect.csv'
    distance = 1000
    expected_filename = ''

    def setUp(self):
        super().setUp()
        self.result_path = os.path.join(os.path.dirname(self.data_folder), self.output_filename)
        self.expected_path = os.path.join(self.repo_folder, 'tests', 'data', self.expected_filename)
        self.input_filename = os.path.join(os.path.basename(self.data_folder), 'elwha_test.shp')
        self.args = dict(
            folder = os.path.dirname(self.data_folder),
            input_filename=self.input_filename,
            output_filename=self.output_filename,
            distance=self.distance,
        )


    def test_rows(self):
        """Test number of output rows"""
        self.notebook_ran()
        self.result_exists()
        import pandas as pd
        expected_n = pd.read_csv(self.expected_path).shape[0]
        result_n = pd.read_csv(self.result_path).shape[0]
        self.assertEqual(result_n, expected_n, f'Output has {result_n} rows instead of {expected_n}')

    def test_columns(self):
        """Test columns"""
        self.notebook_ran()
        self.result_exists()
        import pandas as pd
        expected_cols = set([x.lower() for x in pd.read_csv(self.expected_path).columns])
        result_cols = set([x.lower() for x in pd.read_csv(self.result_path).columns])
        self.assertEqual(result_cols, expected_cols, f'Output columns are {result_cols} instead of {expected_cols}')

    def test_data(self):
        """Test data values"""
        self.notebook_ran()
        self.result_exists()
        import pandas as pd
        expected_df = pd.read_csv(self.expected_path).round(2).rename(columns=str.lower)
        result_df = pd.read_csv(self.result_path).round(2).rename(columns=str.lower).filter(expected_df.columns)
        self.assertTrue(result_df.equals(expected_df), f'Output data appears to be incorrect')


class Problem3A(Problem3):
    suffix = 'a'
    output_filename = 'transect_1000.csv'
    distance = 1000
    expected_filename = 'transect_a.csv'


class Problem3B(Problem3):
    suffix = 'b'
    output_filename = 'transect_500.csv'
    distance = 500
    expected_filename = 'transect_b.csv'


class Problem3R(Problem3):
    expected_filename = 'transect_a.csv'


if __name__ == '__main__':
    test_notebook(1)
    test_notebook(2)
    test_notebook(3)
